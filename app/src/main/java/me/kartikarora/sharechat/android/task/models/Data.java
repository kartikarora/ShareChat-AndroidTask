
package me.kartikarora.sharechat.android.task.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("author_name")
    @Expose
    private String authorName;
    @SerializedName("author_contact")
    @Expose
    private String authorContact;
    @SerializedName("author_dob")
    @Expose
    private String authorDob;
    @SerializedName("author_status")
    @Expose
    private String authorStatus;
    @SerializedName("author_gender")
    @Expose
    private String authorGender;
    @SerializedName("profile_url")
    @Expose
    private String profileUrl;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorContact() {
        return authorContact;
    }

    public void setAuthorContact(String authorContact) {
        this.authorContact = authorContact;
    }

    public String getAuthorDob() {
        return authorDob;
    }

    public void setAuthorDob(String authorDob) {
        this.authorDob = authorDob;
    }

    public String getAuthorStatus() {
        return authorStatus;
    }

    public void setAuthorStatus(String authorStatus) {
        this.authorStatus = authorStatus;
    }

    public String getAuthorGender() {
        return authorGender;
    }

    public void setAuthorGender(String authorGender) {
        this.authorGender = authorGender;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

}
