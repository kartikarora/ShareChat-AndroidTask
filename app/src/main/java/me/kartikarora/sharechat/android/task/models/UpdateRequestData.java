package me.kartikarora.sharechat.android.task.models;

/**
 * Developer: chipset
 * Package : me.kartikarora.sharechat.android.task.models
 * Project : ShareChat_Task
 * Date : 4/29/17
 */

public class UpdateRequestData {

    private String id;
    private String author_name;
    private String author_contact;
    private String author_dob;
    private String author_status;
    private String author_gender;
    private String profile_url;

    public UpdateRequestData(String id, String author_name, String author_contact, String author_dob, String author_status, String author_gender, String profile_url) {
        this.id = id;
        this.author_name = author_name;
        this.author_contact = author_contact;
        this.author_dob = author_dob;
        this.author_status = author_status;
        this.author_gender = author_gender;
        this.profile_url = profile_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthorName() {
        return author_name;
    }

    public void setAuthorName(String author_name) {
        this.author_name = author_name;
    }

    public String getAuthorContact() {
        return author_contact;
    }

    public void setAuthorContact(String author_contact) {
        this.author_contact = author_contact;
    }

    public String getAuthorDob() {
        return author_dob;
    }

    public void setAuthorDob(String author_dob) {
        this.author_dob = author_dob;
    }

    public String getAuthorStatus() {
        return author_status;
    }

    public void setAuthorStatus(String author_status) {
        this.author_status = author_status;
    }

    public String getAuthorGender() {
        return author_gender;
    }

    public void setAuthorGender(String author_gender) {
        this.author_gender = author_gender;
    }

    public String getProfileUrl() {
        return profile_url;
    }

    public void setProfileUrl(String profile_url) {
        this.profile_url = profile_url;
    }
}
