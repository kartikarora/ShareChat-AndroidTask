package me.kartikarora.sharechat.android.task.models;

/**
 * Developer: chipset
 * Package : me.kartikarora.sharechat.android.task.models
 * Project : ShareChat_Task
 * Date : 4/29/17
 */

public class DataRequest {
    private String request_id;
    private int id_offset;

    public DataRequest(String request_id, int id_offset) {
        this.request_id = request_id;
        this.id_offset = id_offset;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public int getId_offset() {
        return id_offset;
    }

    public void setId_offset(int id_offset) {
        this.id_offset = id_offset;
    }
}
