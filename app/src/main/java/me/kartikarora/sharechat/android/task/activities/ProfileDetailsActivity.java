package me.kartikarora.sharechat.android.task.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;

import me.kartikarora.sharechat.android.task.R;
import me.kartikarora.sharechat.android.task.models.Datum;
import me.kartikarora.sharechat.android.task.models.UpdateRequestData;
import me.kartikarora.sharechat.android.task.services.DataFetchService;

import static me.kartikarora.sharechat.android.task.helpers.Constants.ACTION_UPDATE_PROFILE;
import static me.kartikarora.sharechat.android.task.helpers.Constants.ACTION_UPDATE_RESPONSE;
import static me.kartikarora.sharechat.android.task.helpers.Constants.EXTRA_UPDATE_ERROR;
import static me.kartikarora.sharechat.android.task.helpers.Constants.EXTRA_UPDATE_PROFILE;
import static me.kartikarora.sharechat.android.task.helpers.Constants.EXTRA_UPDATE_SUCCESS;
import static me.kartikarora.sharechat.android.task.helpers.Constants.KEY_WRITE_PERM;

public class ProfileDetailsActivity extends AppCompatActivity {

    private UpdateResponseReceiver mReceiver;
    private boolean mChangesSaved = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_details);

        boolean canWrite = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE).getInt(KEY_WRITE_PERM, -1) == 0;
        final Datum user = new Gson().fromJson(getIntent().getStringExtra(EXTRA_UPDATE_PROFILE), Datum.class);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final TextInputLayout contactTextInputLayout = (TextInputLayout) findViewById(R.id.contact_text_input_layout);
        final TextInputLayout statusTextInputLayout = (TextInputLayout) findViewById(R.id.status_text_input_layout);
        final TextInputLayout dobTextInputLayout = (TextInputLayout) findViewById(R.id.dob_text_input_layout);
        final TextInputLayout genderTextInputLayout = (TextInputLayout) findViewById(R.id.gender_text_input_layout);
        final ImageView profileImageView = (ImageView) findViewById(R.id.profile_image_view);
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        Button updateButton = (Button) findViewById(R.id.update_button);

        collapsingToolbarLayout.setTitle(user.getAuthorName());
        contactTextInputLayout.getEditText().setText(user.getAuthorContact());
        statusTextInputLayout.getEditText().setText(user.getAuthorStatus());
        dobTextInputLayout.getEditText().setText(user.getAuthorDob());
        genderTextInputLayout.getEditText().setText(user.getAuthorGender());
        getSupportActionBar().setTitle(user.getAuthorName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (user.getProfileUrl().startsWith("/") && canWrite) {
            Picasso.with(ProfileDetailsActivity.this).load(new File(user.getProfileUrl())).fit().centerCrop().into(profileImageView);
        } else {
            Picasso.with(ProfileDetailsActivity.this).load(user.getProfileUrl()).fit().centerCrop().into(profileImageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(ProfileDetailsActivity.this).load(user.getBackupUrl()).fit().centerCrop().into(profileImageView);
                }
            });
        }
        collapsingToolbarLayout.setExpandedTitleColor(Color.parseColor("#00FFFFFF"));


        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateRequestData requestData = new UpdateRequestData(user.getId(), user.getAuthorName(),
                        contactTextInputLayout.getEditText().getText().toString(), dobTextInputLayout.getEditText().getText().toString(),
                        statusTextInputLayout.getEditText().getText().toString(), genderTextInputLayout.getEditText().getText().toString(),
                        user.getProfileUrl());
                startService(new Intent(ProfileDetailsActivity.this, DataFetchService.class)
                        .setAction(ACTION_UPDATE_PROFILE)
                        .putExtra(EXTRA_UPDATE_PROFILE, new Gson().toJson(requestData, UpdateRequestData.class)));
            }
        });
    }

    public class UpdateResponseReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean success = intent.getBooleanExtra(EXTRA_UPDATE_SUCCESS, true);
            if (success) {
                mChangesSaved = true;
                Toast.makeText(context, R.string.update_success, Toast.LENGTH_SHORT).show();
            } else {
                String error = intent.getStringExtra(EXTRA_UPDATE_ERROR);
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(ACTION_UPDATE_RESPONSE);
        mReceiver = new UpdateResponseReceiver();
        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    @Override
    public void onBackPressed() {
        if (!mChangesSaved) {
            checkDiscard();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (!mChangesSaved) {
                checkDiscard();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkDiscard() {
        new AlertDialog.Builder(ProfileDetailsActivity.this)
                .setMessage("Discard changes?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ProfileDetailsActivity.this.finish();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .create().show();
    }
}
