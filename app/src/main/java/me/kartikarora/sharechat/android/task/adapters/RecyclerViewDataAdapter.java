package me.kartikarora.sharechat.android.task.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import me.kartikarora.sharechat.android.task.R;
import me.kartikarora.sharechat.android.task.activities.ProfileDetailsActivity;
import me.kartikarora.sharechat.android.task.helpers.DataDbHelper;
import me.kartikarora.sharechat.android.task.models.Datum;
import me.kartikarora.sharechat.android.task.services.DataFetchService;

import static me.kartikarora.sharechat.android.task.helpers.Constants.ACTION_FETCH_DATA;
import static me.kartikarora.sharechat.android.task.helpers.Constants.EXTRA_ID_OFFSET;
import static me.kartikarora.sharechat.android.task.helpers.Constants.EXTRA_UPDATE_PROFILE;
import static me.kartikarora.sharechat.android.task.helpers.Constants.KEY_WRITE_PERM;
import static me.kartikarora.sharechat.android.task.helpers.Constants.VIEW_TYPE_IMAGE;
import static me.kartikarora.sharechat.android.task.helpers.Constants.VIEW_TYPE_LOAD_MORE;
import static me.kartikarora.sharechat.android.task.helpers.Constants.VIEW_TYPE_PROFILE;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_ID;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_PROFILE_URL;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_URL;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.TABLE_NAME;

/**
 * Developer: chipset
 * Package : me.kartikarora.sharechat.android.task.adapters
 * Project : ShareChat_Task
 * Date : 4/29/17
 */

public class RecyclerViewDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = RecyclerViewDataAdapter.class.getSimpleName();
    private List<Datum> data;
    private Context context;
    private boolean canWrite;

    public RecyclerViewDataAdapter(Context context, List<Datum> data) {
        this.context = context;
        this.data = data;
        this.canWrite = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE).getInt(KEY_WRITE_PERM, -1) == 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder;
        switch (viewType) {
            case 0:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_user_profile, parent, false);
                viewHolder = new UserProfileViewHolder(view);
                break;
            case 1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_image_post, parent, false);
                viewHolder = new ImagePostViewHolder(view);
                break;
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_load_more, parent, false);
                viewHolder = new LoadMoreViewHolder(view);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                final Datum item1 = data.get(position);
                final UserProfileViewHolder vh1 = (UserProfileViewHolder) holder;
                if (item1.getProfileUrl().startsWith("/") && canWrite) {
                    Picasso.with(context).load(new File(item1.getProfileUrl())).fit().centerCrop().into(vh1.getUserProfileImageView());
                } else {
                    Picasso.with(context).load(item1.getProfileUrl()).into(vh1.getUserProfileImageView(), new Callback() {
                        @Override
                        public void onSuccess() {
                            Picasso.with(context).load(item1.getBackupUrl()).fit().centerCrop().into(vh1.getUserProfileImageView());
                        }

                        @Override
                        public void onError() {

                        }
                    });
                    if (canWrite) {
                        imageDownload(context, item1.getProfileUrl(), item1.getId(), COLUMN_PROFILE_URL, vh1.getUserProfileImageView());
                    }
                }
                vh1.getUserNameTextView().setText(item1.getAuthorName());
                vh1.getUserGenderTextView().setText(item1.getAuthorGender());
                vh1.getUserDOBTextView().setText(item1.getAuthorDob());
                vh1.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.startActivity(new Intent(context, ProfileDetailsActivity.class)
                                .putExtra(EXTRA_UPDATE_PROFILE, new Gson().toJson(item1, Datum.class))
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                });
                break;
            case 1:
                final Datum item2 = data.get(position);
                final ImagePostViewHolder vh2 = (ImagePostViewHolder) holder;
                if (item2.getUrl().startsWith("/") && canWrite) {
                    Picasso.with(context).load(new File(item2.getUrl())).fit().centerCrop().into(vh2.getImagePostImageView());
                } else {
                    Picasso.with(context).load(item2.getUrl()).into(vh2.getImagePostImageView(), new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(context).load(item2.getBackupUrl()).fit().centerCrop().into(vh2.getImagePostImageView());
                        }
                    });
                    if (canWrite) {
                        imageDownload(context, item2.getUrl(), item2.getId(), COLUMN_URL, vh2.getImagePostImageView());
                    }
                }
                vh2.getImagePostAuthorTextView().setText(item2.getAuthorName());
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(item2.getPostedOn());
                vh2.getImagePostDateTextView().setText(calendar.get(Calendar.DAY_OF_MONTH)
                        + "/" + calendar.get(Calendar.MONTH) + "/" + calendar.get(Calendar.YEAR));
                break;
            case 2:
                final LoadMoreViewHolder vh3 = (LoadMoreViewHolder) holder;
                vh3.getLoadMoreButton().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        vh3.getProgressBar().setVisibility(View.VISIBLE);
                        vh3.getLoadMoreButton().setVisibility(View.GONE);
                        context.startService(new Intent(context, DataFetchService.class)
                                .setAction(ACTION_FETCH_DATA).putExtra(EXTRA_ID_OFFSET, data.get(data.size() - 1).getId()));
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                vh3.getProgressBar().setVisibility(View.GONE);
                                vh3.getLoadMoreButton().setVisibility(View.VISIBLE);
                            }
                        }, 2000);
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position).getType().equals(VIEW_TYPE_PROFILE)) {
            return 0;
        } else if (data.get(position).getType().equals(VIEW_TYPE_IMAGE)) {
            return 1;
        } else if (position == data.size() - 1 && data.get(position).getType().equals(VIEW_TYPE_LOAD_MORE)) {
            return 2;
        }
        return -1;
    }

    public void imageDownload(final Context context, final String url, final String id, final String col, ImageView imageView) {
        Target target = new Target() {

            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/ShareChat/" + "IMAGE_" + id + ".jpg");
                        try {
                            file.getParentFile().mkdirs();
                            file.createNewFile();
                            FileOutputStream outputStream = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                            outputStream.flush();
                            outputStream.close();
                            SQLiteDatabase db = new DataDbHelper(context).getWritableDatabase();
                            db.execSQL("UPDATE " + TABLE_NAME + " SET " + col + " = \'" + file.getAbsolutePath() + "\' WHERE " + COLUMN_ID + " = \'" + id + "\';");
                            db.close();
                        } catch (IOException e) {
                            Log.e("IOException", e.getLocalizedMessage());
                        }
                    }
                }).start();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

        Picasso.with(context).load(url).into(target);
        imageView.setTag(target);
    }
}


class UserProfileViewHolder extends RecyclerView.ViewHolder {
    private ImageView userProfileImageView;
    private TextView userNameTextView;
    private TextView userGenderTextView;
    private TextView userDOBTextView;

    public UserProfileViewHolder(View view) {
        super(view);
        this.userProfileImageView = (ImageView) view.findViewById(R.id.user_profile_image_view);
        this.userNameTextView = (TextView) view.findViewById(R.id.user_name_text_view);
        this.userGenderTextView = (TextView) view.findViewById(R.id.user_gender_text_view);
        this.userDOBTextView = (TextView) view.findViewById(R.id.user_dob_text_view);
    }

    public ImageView getUserProfileImageView() {
        return userProfileImageView;
    }

    public TextView getUserNameTextView() {
        return userNameTextView;
    }

    public TextView getUserGenderTextView() {
        return userGenderTextView;
    }

    public TextView getUserDOBTextView() {
        return userDOBTextView;
    }
}


class ImagePostViewHolder extends RecyclerView.ViewHolder {
    private TextView imagePostAuthorTextView;
    private TextView imagePostDateTextView;
    private ImageView imagePostImageView;

    public ImagePostViewHolder(View view) {
        super(view);
        this.imagePostAuthorTextView = (TextView) view.findViewById(R.id.image_post_author_text_view);
        this.imagePostDateTextView = (TextView) view.findViewById(R.id.image_post_date_text_view);
        this.imagePostImageView = (ImageView) view.findViewById(R.id.image_post_image_view);
    }

    public TextView getImagePostAuthorTextView() {
        return imagePostAuthorTextView;
    }

    public TextView getImagePostDateTextView() {
        return imagePostDateTextView;
    }

    public ImageView getImagePostImageView() {
        return imagePostImageView;
    }
}


class LoadMoreViewHolder extends RecyclerView.ViewHolder {
    private Button loadMoreButton;
    private ProgressBar progressBar;

    public LoadMoreViewHolder(View view) {
        super(view);
        loadMoreButton = (Button) view.findViewById(R.id.load_more_button);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
    }

    public Button getLoadMoreButton() {
        return loadMoreButton;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }
}
