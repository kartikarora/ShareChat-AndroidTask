package me.kartikarora.sharechat.android.task.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.util.ArrayList;

import me.kartikarora.sharechat.android.task.R;
import me.kartikarora.sharechat.android.task.adapters.RecyclerViewDataAdapter;
import me.kartikarora.sharechat.android.task.helpers.DataDbHelper;
import me.kartikarora.sharechat.android.task.models.Datum;
import me.kartikarora.sharechat.android.task.services.DataFetchService;

import static me.kartikarora.sharechat.android.task.helpers.Constants.ACTION_DATA_RESPONSE;
import static me.kartikarora.sharechat.android.task.helpers.Constants.ACTION_FETCH_DATA;
import static me.kartikarora.sharechat.android.task.helpers.Constants.EXTRA_DATA_ERROR;
import static me.kartikarora.sharechat.android.task.helpers.Constants.EXTRA_DATA_SIZE;
import static me.kartikarora.sharechat.android.task.helpers.Constants.EXTRA_DATA_SUCCESS;
import static me.kartikarora.sharechat.android.task.helpers.Constants.KEY_WRITE_PERM;
import static me.kartikarora.sharechat.android.task.helpers.Constants.VIEW_TYPE_LOAD_MORE;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_AUTHOR_CONTACT;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_AUTHOR_DOB;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_AUTHOR_GENDER;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_AUTHOR_NAME;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_AUTHOR_STATUS;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_BACKUP_URL;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_ID;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_POSTED_ON;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_PROFILE_URL;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_TYPE;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_URL;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.TABLE_NAME;

public class HomeActivity extends AppCompatActivity {
    private static final int REQEUST_CODE = 1234;
    private static final String TAG = HomeActivity.class.getSimpleName();
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerViewDataAdapter mRecyclerViewDataAdapter;
    private ArrayList<Datum> mDatumArrayList = new ArrayList<>();
    private DataResponseReceiver mReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Stetho.initializeWithDefaults(this);

        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQEUST_CODE);
        } else {
            startService(new Intent(HomeActivity.this, DataFetchService.class)
                    .setAction(ACTION_FETCH_DATA));
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE).edit();
            editor.putInt("write_perm", 0);
            editor.apply();
            startService(new Intent(HomeActivity.this, DataFetchService.class)
                    .setAction(ACTION_FETCH_DATA));
        }

        getSupportActionBar().setTitle(R.string.title_home_activity);

        mRecyclerViewDataAdapter = new RecyclerViewDataAdapter(HomeActivity.this, mDatumArrayList);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(HomeActivity.this);
        mSwipeRefreshLayout.setRefreshing(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation()));
        recyclerView.setAdapter(mRecyclerViewDataAdapter);


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mDatumArrayList.clear();
                SQLiteDatabase db = new DataDbHelper(getApplicationContext()).getWritableDatabase();
                db.execSQL("DELETE FROM " + TABLE_NAME);
                mRecyclerViewDataAdapter.notifyDataSetChanged();
                startService(new Intent(HomeActivity.this, DataFetchService.class)
                        .setAction(ACTION_FETCH_DATA));
            }
        });
    }

    public class DataResponseReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            mSwipeRefreshLayout.setRefreshing(false);
            boolean success = intent.getBooleanExtra(EXTRA_DATA_SUCCESS, true);
            if (!success) {
                String error = intent.getStringExtra(EXTRA_DATA_ERROR);
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            } else {
                if (mDatumArrayList.size() > 0 && mDatumArrayList.get(mDatumArrayList.size() - 1).getType().equals("load_more")) {
                    mDatumArrayList.remove(mDatumArrayList.size() - 1);
                    mRecyclerViewDataAdapter.notifyDataSetChanged();
                }
                SQLiteDatabase db = new DataDbHelper(getApplicationContext()).getReadableDatabase();
                Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
                mDatumArrayList.clear();
                while (cursor.moveToNext()) {
                    Datum datum = new Datum();
                    datum.setType(cursor.getString(cursor.getColumnIndex(COLUMN_TYPE)));
                    datum.setId(cursor.getString(cursor.getColumnIndex(COLUMN_ID)));
                    datum.setUrl(cursor.getString(cursor.getColumnIndex(COLUMN_URL)));
                    datum.setAuthorName(cursor.getString(cursor.getColumnIndex(COLUMN_AUTHOR_NAME)));
                    datum.setPostedOn(cursor.getLong(cursor.getColumnIndex(COLUMN_POSTED_ON)));
                    datum.setAuthorContact(cursor.getString(cursor.getColumnIndex(COLUMN_AUTHOR_CONTACT)));
                    datum.setAuthorDob(cursor.getString(cursor.getColumnIndex(COLUMN_AUTHOR_DOB)));
                    datum.setAuthorStatus(cursor.getString(cursor.getColumnIndex(COLUMN_AUTHOR_STATUS)));
                    datum.setAuthorGender(cursor.getString(cursor.getColumnIndex(COLUMN_AUTHOR_GENDER)));
                    datum.setProfileUrl(cursor.getString(cursor.getColumnIndex(COLUMN_PROFILE_URL)));
                    datum.setBackupUrl(cursor.getString(cursor.getColumnIndex(COLUMN_BACKUP_URL)));
                    mDatumArrayList.add(datum);
                }
                cursor.close();
                db.close();
                if (getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE).getInt(EXTRA_DATA_SIZE, 5) > 0) {
                    Datum loadMore = new Datum();
                    loadMore.setType(VIEW_TYPE_LOAD_MORE);
                    loadMore.setId(mDatumArrayList.get(mDatumArrayList.size() - 1).getId());
                    mDatumArrayList.add(loadMore);
                }
                mRecyclerViewDataAdapter.notifyDataSetChanged();
                Toast.makeText(context, R.string.new_items, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(ACTION_DATA_RESPONSE);
        mReceiver = new DataResponseReceiver();
        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQEUST_CODE && grantResults.length > 0) {
            SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE).edit();
            editor.putInt(KEY_WRITE_PERM, grantResults[0]);
            editor.apply();

        }
        startService(new Intent(HomeActivity.this, DataFetchService.class)
                .setAction(ACTION_FETCH_DATA));
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
