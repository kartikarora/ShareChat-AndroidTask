package me.kartikarora.sharechat.android.task.models;

/**
 * Developer: chipset
 * Package : me.kartikarora.sharechat.android.task.models
 * Project : ShareChat_Task
 * Date : 4/29/17
 */

public class UpdateRequest {

    private String request_id;
    private UpdateRequestData data;

    public UpdateRequest(String request_id, UpdateRequestData data) {
        this.request_id = request_id;
        this.data = data;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public UpdateRequestData getData() {
        return data;
    }

    public void setData(UpdateRequestData data) {
        this.data = data;
    }
}
