package me.kartikarora.sharechat.android.task.helpers;

import android.provider.BaseColumns;

/**
 * Developer: chipset
 * Package : me.kartikarora.sharechat.android.task.helpers
 * Project : ShareChat_Task
 * Date : 5/1/17
 */

public class DataEntry implements BaseColumns {
    public static final String TABLE_NAME = "data";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_URL = "url";
    public static final String COLUMN_AUTHOR_NAME = "author_name";
    public static final String COLUMN_POSTED_ON = "postedOn";
    public static final String COLUMN_AUTHOR_CONTACT = "author_contact";
    public static final String COLUMN_AUTHOR_DOB = "author_dob";
    public static final String COLUMN_AUTHOR_STATUS = "author_status";
    public static final String COLUMN_AUTHOR_GENDER = "author_gender";
    public static final String COLUMN_PROFILE_URL = "profile_url";
    public static final String COLUMN_BACKUP_URL = "backup_url";
}
