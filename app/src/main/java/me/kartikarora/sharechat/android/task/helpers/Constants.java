package me.kartikarora.sharechat.android.task.helpers;

/**
 * Developer: chipset
 * Package : me.kartikarora.sharechat.android.task.helpers
 * Project : ShareChat_Task
 * Date : 5/1/17
 */

public class Constants {

    public static final String ACTION_FETCH_DATA = "me.kartikarora.sharechat.android.task.action.FETCH";
    public static final String ACTION_UPDATE_PROFILE = "me.kartikarora.sharechat.android.task.action.UPDATE";
    public static final String ACTION_DATA_RESPONSE = "me.kartikarora.sharechat.android.task.action.data.RESPONSE";
    public static final String ACTION_UPDATE_RESPONSE = "me.kartikarora.sharechat.android.task.action.update.RESPONSE";

    public static final String EXTRA_DATA_SIZE = "data_size";
    public static final String EXTRA_DATA_SUCCESS = "data_success";
    public static final String EXTRA_DATA_ERROR = "data_error";
    public static final String EXTRA_UPDATE_PROFILE = "update_profile";
    public static final String EXTRA_UPDATE_SUCCESS = "update_success";
    public static final String EXTRA_UPDATE_ERROR = "update_error";
    public static final String EXTRA_ID_OFFSET = "id_offset";

    public static final String VIEW_TYPE_LOAD_MORE = "load_more";
    public static final String VIEW_TYPE_PROFILE = "profile";
    public static final String VIEW_TYPE_IMAGE = "image";

    public static final String KEY_WRITE_PERM = "write_perm";
}
