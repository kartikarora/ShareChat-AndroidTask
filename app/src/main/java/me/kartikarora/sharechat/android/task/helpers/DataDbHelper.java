package me.kartikarora.sharechat.android.task.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_AUTHOR_CONTACT;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_AUTHOR_DOB;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_AUTHOR_GENDER;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_AUTHOR_NAME;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_AUTHOR_STATUS;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_ID;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_POSTED_ON;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_PROFILE_URL;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_TYPE;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_URL;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_BACKUP_URL;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.TABLE_NAME;


/**
 * Developer: chipset
 * Package : me.kartikarora.sharechat.android.task.helpers
 * Project : ShareChat_Task
 * Date : 5/1/17
 */

public class DataDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "data.db";

    public DataDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID + " TEXT PRIMARY KEY, " +
                COLUMN_AUTHOR_NAME + " TEXT, " +
                COLUMN_TYPE + " TEXT, " +
                COLUMN_URL + " TEXT, " +
                COLUMN_POSTED_ON + " INTEGER, " +
                COLUMN_AUTHOR_CONTACT + " TEXT, " +
                COLUMN_AUTHOR_DOB + " TEXT, " +
                COLUMN_AUTHOR_STATUS + " TEXT," +
                COLUMN_AUTHOR_GENDER + " TEXT," +
                COLUMN_PROFILE_URL + " TEXT," +
                COLUMN_BACKUP_URL + " TEXT" +
                ");";
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + TABLE_NAME + ";");
        onCreate(db);
    }
}
