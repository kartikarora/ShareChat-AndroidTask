package me.kartikarora.sharechat.android.task.services;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;

import com.google.gson.Gson;

import me.kartikarora.sharechat.android.task.R;
import me.kartikarora.sharechat.android.task.helpers.DataDbHelper;
import me.kartikarora.sharechat.android.task.models.DataRequest;
import me.kartikarora.sharechat.android.task.models.DataResponse;
import me.kartikarora.sharechat.android.task.models.Datum;
import me.kartikarora.sharechat.android.task.models.UpdateRequest;
import me.kartikarora.sharechat.android.task.models.UpdateRequestData;
import me.kartikarora.sharechat.android.task.models.UpdateResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

import static me.kartikarora.sharechat.android.task.helpers.Constants.ACTION_DATA_RESPONSE;
import static me.kartikarora.sharechat.android.task.helpers.Constants.ACTION_FETCH_DATA;
import static me.kartikarora.sharechat.android.task.helpers.Constants.ACTION_UPDATE_PROFILE;
import static me.kartikarora.sharechat.android.task.helpers.Constants.ACTION_UPDATE_RESPONSE;
import static me.kartikarora.sharechat.android.task.helpers.Constants.EXTRA_DATA_ERROR;
import static me.kartikarora.sharechat.android.task.helpers.Constants.EXTRA_DATA_SIZE;
import static me.kartikarora.sharechat.android.task.helpers.Constants.EXTRA_DATA_SUCCESS;
import static me.kartikarora.sharechat.android.task.helpers.Constants.EXTRA_ID_OFFSET;
import static me.kartikarora.sharechat.android.task.helpers.Constants.EXTRA_UPDATE_ERROR;
import static me.kartikarora.sharechat.android.task.helpers.Constants.EXTRA_UPDATE_PROFILE;
import static me.kartikarora.sharechat.android.task.helpers.Constants.EXTRA_UPDATE_SUCCESS;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_AUTHOR_CONTACT;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_AUTHOR_DOB;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_AUTHOR_GENDER;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_AUTHOR_NAME;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_AUTHOR_STATUS;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_BACKUP_URL;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_ID;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_POSTED_ON;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_PROFILE_URL;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_TYPE;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.COLUMN_URL;
import static me.kartikarora.sharechat.android.task.helpers.DataEntry.TABLE_NAME;


/**
 * Developer: chipset
 * Package : me.kartikarora.sharechat.android.task.services
 * Project : ShareChat_Task
 * Date : 4/29/17
 */

public class DataFetchService extends IntentService {

    private static final String BASE_URL = "http://35.154.249.234:5000/";
    private static final String REQUEST_ID = "c00a6eb6cf19d5cc060b0f640ae9d2206b740ebf";
    private static final String TAG = DataFetchInterface.class.getSimpleName();
    private static DataFetchInterface sDataFetchInterface = null;

    public DataFetchService() {
        super("DataFetchService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (sDataFetchInterface == null) {
            sDataFetchInterface = getDataFetchInterface();
        }
        assert intent != null;
        if (intent.getAction().equals(ACTION_FETCH_DATA)) {
            final String id = intent.getStringExtra(EXTRA_ID_OFFSET);
            DataRequest dataRequestBody = new DataRequest(REQUEST_ID, id == null ? -1 : Integer.parseInt(id));
            sDataFetchInterface.getData(dataRequestBody).enqueue(new Callback<DataResponse>() {
                @Override
                public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {
                    if (response.body().isSuccess()) {
                        SQLiteDatabase db = new DataDbHelper(getApplicationContext()).getWritableDatabase();
                        for (Datum item : response.body().getData()) {
                            String backupUrl = item.getUrl() != null ? item.getUrl() : item.getProfileUrl();
                            ContentValues values = new ContentValues();
                            values.put(COLUMN_TYPE, item.getType());
                            values.put(COLUMN_ID, item.getId());
                            values.put(COLUMN_URL, item.getUrl());
                            values.put(COLUMN_AUTHOR_NAME, item.getAuthorName());
                            values.put(COLUMN_POSTED_ON, item.getPostedOn());
                            values.put(COLUMN_AUTHOR_CONTACT, item.getAuthorContact());
                            values.put(COLUMN_AUTHOR_DOB, item.getAuthorDob());
                            values.put(COLUMN_AUTHOR_STATUS, item.getAuthorStatus());
                            values.put(COLUMN_AUTHOR_GENDER, item.getAuthorGender());
                            values.put(COLUMN_PROFILE_URL, item.getProfileUrl());
                            values.put(COLUMN_BACKUP_URL, backupUrl);
                            db.beginTransaction();
                            try {
                                db.insert(TABLE_NAME, null, values);
                            } catch (SQLiteConstraintException e) {
                                e.printStackTrace();
                            }
                            db.setTransactionSuccessful();
                            db.endTransaction();
                        }
                        db.close();
                    }
                    SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE).edit();
                    editor.putInt(EXTRA_DATA_SIZE, response.body().getData().size());
                    editor.apply();
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(ACTION_DATA_RESPONSE);
                    broadcastIntent.putExtra(EXTRA_DATA_SUCCESS, response.body().isSuccess());
                    broadcastIntent.putExtra(EXTRA_DATA_ERROR, response.body().getError());
                    sendBroadcast(broadcastIntent);
                }

                @Override
                public void onFailure(Call<DataResponse> call, Throwable t) {
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(ACTION_DATA_RESPONSE);
                    broadcastIntent.putExtra(EXTRA_DATA_SUCCESS, false);
                    broadcastIntent.putExtra(EXTRA_DATA_ERROR, "Something went wrong");
                    sendBroadcast(broadcastIntent);
                    t.printStackTrace();
                }
            });
        } else if (intent.getAction().equals(ACTION_UPDATE_PROFILE)) {
            final UpdateRequestData profile = new Gson().fromJson(intent.getStringExtra(EXTRA_UPDATE_PROFILE), UpdateRequestData.class);
            final UpdateRequest updateRequestBody = new UpdateRequest(REQUEST_ID, profile);
            sDataFetchInterface.updateProfile(updateRequestBody).enqueue(new Callback<UpdateResponse>() {
                @Override
                public void onResponse(Call<UpdateResponse> call, Response<UpdateResponse> response) {
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(ACTION_UPDATE_RESPONSE);
                    broadcastIntent.putExtra(EXTRA_UPDATE_SUCCESS, response.body().isSuccess());
                    broadcastIntent.putExtra(EXTRA_UPDATE_ERROR, response.body().getError());
                    SQLiteDatabase db = new DataDbHelper(getApplicationContext()).getWritableDatabase();
                    db.execSQL("UPDATE " + TABLE_NAME + "SET " + COLUMN_AUTHOR_CONTACT + " = " + profile.getAuthorContact() + ", "
                            + COLUMN_AUTHOR_DOB + " = " + profile.getAuthorDob() + ", "
                            + COLUMN_AUTHOR_GENDER + " = " + profile.getAuthorGender() + ", "
                            + COLUMN_AUTHOR_STATUS + " = " + profile.getAuthorStatus() + ", "
                            + "WHERE " + COLUMN_ID + " = " + profile.getId() + ";");
                    sendBroadcast(broadcastIntent);
                }

                @Override
                public void onFailure(Call<UpdateResponse> call, Throwable t) {
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(ACTION_UPDATE_RESPONSE);
                    broadcastIntent.putExtra(EXTRA_UPDATE_SUCCESS, false);
                    broadcastIntent.putExtra(EXTRA_UPDATE_ERROR, "Something went wrong");
                    sendBroadcast(broadcastIntent);
                    t.printStackTrace();
                }
            });
        }
    }

    private DataFetchInterface getDataFetchInterface() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(DataFetchInterface.class);
    }


    private interface DataFetchInterface {
        @Headers("Content-type: application/json")
        @POST("data")
        Call<DataResponse> getData(@Body DataRequest data);

        @Headers("Content-type: application/json")
        @POST("update")
        Call<UpdateResponse> updateProfile(@Body UpdateRequest update);

    }
}